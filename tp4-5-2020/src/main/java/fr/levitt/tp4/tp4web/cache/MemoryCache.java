package fr.levitt.tp4.tp4web.cache;

import fr.levitt.tp4.tp4web.core.Gares;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MemoryCache<T> implements CustomCache<T> {

    private T cache;

    public T getCache() {
        return cache;
    }

    public void updateCache(T cache) {
        this.cache = cache;
    }

    public void invalidate() {
        this.cache = null;
    }
}
