package fr.levitt.tp4.tp4web.cache;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

@Service
@Primary
public class FileCache<T> implements CustomCache<T> {

    @Autowired
    private ObjectMapper mapper;

    @Override
    public T getCache() {
        // Lire le fichier et le convertir en T
        return null;
    }

    @Override
    public void updateCache(T cache) {
        // Sauvegarder T sous forme de fichier
        File file = new File("/home/gon/Desktop/"+cache.getClass().getName()+".json");
        try {
            mapper.writeValue(file, cache);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void invalidate() {
        // Supprimer le fichier
    }
}
