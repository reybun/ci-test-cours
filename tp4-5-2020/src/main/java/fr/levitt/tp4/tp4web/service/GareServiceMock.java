package fr.levitt.tp4.tp4web.service;

import fr.levitt.tp4.tp4web.core.Gare;
import fr.levitt.tp4.tp4web.core.Gares;
import fr.levitt.tp4.tp4web.core.TableauAffichage;
import fr.levitt.tp4.tp4web.core.Train;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class GareServiceMock implements GareService {

    public TableauAffichage getTableauDeparts(String idGare) {
        return null;
    }

    @Override
    public Gares getGares() {
        return null;
    }
}
