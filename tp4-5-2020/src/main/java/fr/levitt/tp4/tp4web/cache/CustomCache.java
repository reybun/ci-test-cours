package fr.levitt.tp4.tp4web.cache;

public interface CustomCache<T> {
    public T getCache();
    public void updateCache(T cache);
    public void invalidate();
}
