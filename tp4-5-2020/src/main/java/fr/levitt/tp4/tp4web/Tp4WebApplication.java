package fr.levitt.tp4.tp4web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class Tp4WebApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tp4WebApplication.class, args);
	}

}
